provider "vsphere" {
  user                 = var.vsphere_user
  password             = var.vsphere_password
  vsphere_server       = var.vsphere_vcenter
  allow_unverified_ssl = true
}

data "vsphere_datacenter" "dc" {
  name = var.vsphere_datacenter
}

data "vsphere_compute_cluster" "cluster" {
  name              = var.vsphere_vm_compute_cluster
  datacenter_id     = data.vsphere_datacenter.dc.id
}

data "vsphere_datastore" "datastore" {
  name          = var.vsphere_datastore
  datacenter_id = data.vsphere_datacenter.dc.id
}

# data "vsphere_resource_pool" "pool" {
#   name          = var.vsphere_resource_pool
#   datacenter_id = data.vsphere_datacenter.dc.id
# }

data "vsphere_network" "network" {
  name          = var.cluster_name
  datacenter_id = data.vsphere_datacenter.dc.id
  depends_on    = [nsxt_policy_segment.kubernetes]
}

data "vsphere_virtual_machine" "template" {
  name          = var.vsphere_virtual_machine_template
  datacenter_id = data.vsphere_datacenter.dc.id
}

# Create a folder for the Rancher Cluster
resource "vsphere_folder" "rancher_cluster" {
  type          = "vm"
  path          = "${var.vsphere_folder}/${var.cluster_name}"
  datacenter_id = data.vsphere_datacenter.dc.id
}

resource "vsphere_virtual_machine" "rke" {
  name             = "${var.rancher_hostname_master}-${count.index + 1}"
  count            = var.rancher_count_master
  resource_pool_id = data.vsphere_compute_cluster.cluster.resource_pool_id
  datastore_id     = data.vsphere_datastore.datastore.id
  folder           = vsphere_folder.rancher_cluster.path
  num_cpus         = 2
  memory           = 4096
  guest_id         = data.vsphere_virtual_machine.template.guest_id

  scsi_type = data.vsphere_virtual_machine.template.scsi_type

  enable_disk_uuid = true

  network_interface {
    network_id = data.vsphere_network.network.id
  }

  disk {
    label            = "disk0"
    size             = 64
    eagerly_scrub    = data.vsphere_virtual_machine.template.disks[0].eagerly_scrub
    thin_provisioned = data.vsphere_virtual_machine.template.disks[0].thin_provisioned
    unit_number      = 0
  }

  clone {
    template_uuid = data.vsphere_virtual_machine.template.id
  }
  
  extra_config = {
    "guestinfo.metadata"          = base64gzip(data.template_file.cloud_metadata_master[count.index].rendered)
    "guestinfo.metadata.encoding" = "gzip+base64"
    "guestinfo.userdata"          = base64gzip(data.template_file.userdata_master.rendered)
    "guestinfo.userdata.encoding" = "gzip+base64"
  }
  lifecycle {
    ignore_changes  = [extra_config,clone]
  }
}

# resource "vsphere_virtual_machine" "rke-worker" {
#   name             = "${var.rancher_hostname_worker}-${count.index + 1}"
#   count            = var.rancher_count_worker
#   resource_pool_id = data.vsphere_compute_cluster.cluster.resource_pool_id
#   datastore_id     = data.vsphere_datastore.datastore.id
#   folder           = vsphere_folder.rancher_cluster.path
#   num_cpus         = 4
#   memory           = 8192
#   guest_id         = data.vsphere_virtual_machine.template.guest_id

#   scsi_type = data.vsphere_virtual_machine.template.scsi_type

#   network_interface {
#     network_id = data.vsphere_network.network.id
#   }
  
#   disk {
#     label            = "disk0"
#     size             = 64
#     eagerly_scrub    = data.vsphere_virtual_machine.template.disks[0].eagerly_scrub
#     thin_provisioned = data.vsphere_virtual_machine.template.disks[0].thin_provisioned
#     unit_number      = 0
#   }


#   clone {
#     template_uuid = data.vsphere_virtual_machine.template.id
#   }
#   extra_config = {
#   "guestinfo.metadata"          = base64gzip(data.template_file.cloud_metadata_worker[count.index].rendered)
#   "guestinfo.metadata.encoding" = "gzip+base64"
#   "guestinfo.userdata"          = base64gzip(data.template_file.userdata_worker.rendered)
#   "guestinfo.userdata.encoding" = "gzip+base64"
#   }
#   lifecycle {
#   ignore_changes  = [extra_config]
#   }
# }