# Rancher
rancher_count_master = 3
rancher_count_worker = 0
rancher_hostname_master = "rancher-server"
rancher_hostname_worker = "honolulu-rke-w"
rancher_api_url = "rancher.hawaiian.islands.io"
cluster_name = "rancher-server"


# vShpere
vsphere_user = "gitlab@hawaiian.islands.io"
vsphere_vcenter = "vcenter.hawaiian.islands.io"
vsphere_datacenter = "nest"
vsphere_datastore = "vsanDatastore"
vsphere_vm_compute_cluster = "islands"
vsphere_resource_pool = "Resources"
vsphere_network = "trusted"
vsphere_virtual_machine_template = "ubuntu-20.04"
vsphere_folder = "kubernetes"

# NSX
nsx_host = "nsx.hawaiian.islands.io"
nsx_user = "gitlab@hawaiian.islands.io"
nsxt_dhcp_servers = ["172.29.0.1/30"]
nsxt_segment_subnet = "10.1.110.254/24"
nsxt_segment_dhcp_subnets = ["10.1.110.0-10.1.110.127"]
dns_servers = ["192.168.251.145"]

kube_api_ip = "10.0.170.3"
