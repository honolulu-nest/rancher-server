data "template_file" "ctl_cloud_network_master" {
  count    = var.rancher_count_master
  template = file("${path.module}/cloud-init/network-config.yaml")
  # vars = {
  #   ip         = element(split(",", var.rancher_ip_master), count.index)
  #   netmask    = var.rancher_netmask
  #   gateway    = var.rancher_gateway
  #   DNS1       = element(split(",", var.rancher_dns), 0)
  #   DNS2       = element(split(",", var.rancher_dns), 1)
  #   DNS_SEARCH = var.rancher_dns_search
  # }
}

data "template_file" "ctl_cloud_network_worker" {
  count    = var.rancher_count_worker
  template = file("${path.module}/cloud-init/network-config.yaml")
  # vars = {
  #   ip         = element(split(",", var.rancher_ip_worker), count.index)
  #   netmask    = var.rancher_netmask
  #   gateway    = var.rancher_gateway
  #   DNS1       = element(split(",", var.rancher_dns), 0)
  #   DNS2       = element(split(",", var.rancher_dns), 1)
  #   DNS_SEARCH = var.rancher_dns_search
  # }
}

data "template_file" "cloud_metadata_master" {
  count    = var.rancher_count_master
  template = file("${path.module}/cloud-init/metadata.json")
  vars = {
    hostname = "${var.rancher_hostname_master}-${count.index + 1}"
    network  = base64gzip(data.template_file.ctl_cloud_network_master[count.index].rendered)
  }
}

data "template_file" "cloud_metadata_worker" {
  count    = var.rancher_count_worker
  template = file("${path.module}/cloud-init/metadata.json")
  vars = {
    hostname = "${var.rancher_hostname_worker}-${count.index + 1}"
    network  = base64gzip(data.template_file.ctl_cloud_network_worker[count.index].rendered)
  }
}

data "template_file" "userdata_master" {
  template = file("${path.module}/cloud-init/cloud-config.yaml")
}

data "template_file" "userdata_worker" {
  template = file("${path.module}/cloud-init/cloud-config.yaml")
}

