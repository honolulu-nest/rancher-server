provider "helm" {
  kubernetes {
    host = rke_cluster.cluster.api_server_url

    client_certificate = rke_cluster.cluster.client_cert
    client_key = rke_cluster.cluster.client_key
    cluster_ca_certificate = rke_cluster.cluster.ca_crt
    load_config_file = false
  }
}

resource "helm_release" "cert-manager" {
    depends_on = [rke_cluster.cluster]
    repository = "https://charts.jetstack.io"
    
    name = "cert-manager"
    namespace = "cert-manager"
    chart = "cert-manager"

    set {
        name = "installCRDs"
        value = true
    }
}


resource "helm_release" "rancher" {
    depends_on = [helm_release.cert-manager]
    repository = "https://releases.rancher.com/server-charts/latest"
    
    name = "rancher"
    namespace = "cattle-system"
    chart = "rancher"

    set {
        name = "hostname"
        value = var.rancher_api_url
    }
}

