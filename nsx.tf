provider "nsxt" {
    host                 = var.nsx_host
    username             = var.vsphere_user
    password             = var.vsphere_password
    allow_unverified_ssl = true
}

data "nsxt_policy_transport_zone" "overlay_transport_zone" {
  display_name = var.nsxt_overlay_transport_zone
}

data "nsxt_policy_edge_cluster" "EC" {
  display_name = var.nsxt_edge_cluster
}

data "nsxt_policy_tier0_gateway" "t0_gateway" {
  display_name = var.nsxt_t0
}

resource "nsxt_policy_dhcp_server" "tier_dhcp" {
  display_name     = "tier_dhcp"
  description      = "DHCP server for ${var.cluster_name}"
  server_addresses = var.nsxt_dhcp_servers
}

# Tier1 Gateway
resource "nsxt_policy_tier1_gateway" "t1_gateway" {
  display_name              = "${var.cluster_name}_t1"
  description               = "Tier1 provisioned by Terraform"
  edge_cluster_path         = data.nsxt_policy_edge_cluster.EC.path
  dhcp_config_path          = nsxt_policy_dhcp_server.tier_dhcp.path
  failover_mode             = "PREEMPTIVE"
  default_rule_logging      = "false"
  enable_firewall           = "true"
  enable_standby_relocation = "false"
  force_whitelisting        = "false"
  tier0_path                = data.nsxt_policy_tier0_gateway.t0_gateway.path
  route_advertisement_types = ["TIER1_STATIC_ROUTES", "TIER1_CONNECTED", "TIER1_NAT", "TIER1_LB_VIP"]
  pool_allocation           = "ROUTING"
}

# Segment
resource "nsxt_policy_segment" "kubernetes" {
  display_name        = var.cluster_name
  description         = "Terraform provisioned ${var.cluster_name} Segment"
  connectivity_path   = nsxt_policy_tier1_gateway.t1_gateway.path
  transport_zone_path = data.nsxt_policy_transport_zone.overlay_transport_zone.path

  subnet {
    cidr        = var.nsxt_segment_subnet
    dhcp_ranges = var.nsxt_segment_dhcp_subnets

    dhcp_v4_config {
      server_address = element(var.nsxt_dhcp_servers, 0)
      lease_time     = 36000
      dns_servers    = var.dns_servers
    }
  }
}

# Tagging
resource "nsxt_policy_vm_tags" "rke" {
  count = var.rancher_count_master
  instance_id = vsphere_virtual_machine.rke[count.index].id
  tag {
      scope = "k8s/${var.cluster_name}"
      tag = "master"
  }
  tag {
      scope = "k8s/${var.cluster_name}"
      tag = "worker"
  }
}

# Tag Groups
resource "nsxt_policy_group" "kube_api" {
  display_name = var.cluster_name
  description  = "Group consisting of ALL ${var.cluster_name} Master VMs"
  criteria {
    condition {
      member_type = "VirtualMachine"
      operator    = "EQUALS"
      key         = "Tag"
      value       = "k8s/${var.cluster_name}|master"
    }
  }
}

resource "nsxt_policy_group" "ingress" {
  display_name = var.cluster_name
  description  = "Group consisting of ALL ${var.cluster_name} Worker VMs"
  criteria {
    condition {
      member_type = "VirtualMachine"
      operator    = "EQUALS"
      key         = "Tag"
      value       = "k8s/${var.cluster_name}|worker"
    }
  }
}

# LB Service
resource "nsxt_policy_lb_service" "kubernetes" {
  display_name      = var.cluster_name
  description       = "Terraform provisioned Service"
  connectivity_path = nsxt_policy_tier1_gateway.t1_gateway.path
  size              = "SMALL"
  enabled           = true
  error_log_level   = "ERROR"
}

# LB Pools
resource "nsxt_policy_lb_pool" "kube_api" {
  display_name         = "${var.cluster_name}_kube_api"
  description          = "Terraform provisioned LB Pool"
  algorithm            = "LEAST_CONNECTION"
  active_monitor_path  = "/infra/lb-monitor-profiles/kube-api"
  member_group {
    group_path                 = nsxt_policy_group.kube_api.path
    port                       = "6443"
  }
  snat {
    type = "AUTOMAP"
  }
}

resource "nsxt_policy_lb_pool" "ingress_https" {
  display_name         = "${var.cluster_name}_ingress_https"
  description          = "Terraform provisioned LB Pool"
  algorithm            = "LEAST_CONNECTION"
  active_monitor_path  = "/infra/lb-monitor-profiles/default-https-lb-monitor"
  member_group {
    group_path                 = nsxt_policy_group.ingress.path
    port                       = "443"
  }
  snat {
    type = "AUTOMAP"
  }
}

resource "nsxt_policy_lb_pool" "ingress_http" {
  display_name         = "${var.cluster_name}_ingress_http"
  description          = "Terraform provisioned LB Pool"
  algorithm            = "LEAST_CONNECTION"
  active_monitor_path  = "/infra/lb-monitor-profiles/default-http-lb-monitor"
  member_group {
    group_path                 = nsxt_policy_group.ingress.path
    port                       = "80"
  }
  snat {
    type = "AUTOMAP"
  }
}

# Virtual Server
resource "nsxt_policy_lb_virtual_server" "kube_api" {
  display_name               = "${var.cluster_name}_kube_api"
  description                = "Terraform provisioned Virtual Server"
  access_log_enabled         = true
  application_profile_path   = "/infra/lb-app-profiles/default-tcp-lb-app-profile"
  enabled                    = true
  ip_address                 = var.kube_api_ip
  ports                      = ["6443"]
  service_path               = nsxt_policy_lb_service.kubernetes.path
  pool_path                  = nsxt_policy_lb_pool.kube_api.path
}

resource "nsxt_policy_lb_virtual_server" "ingress_https" {
  display_name               = "${var.cluster_name}_ingress_https"
  description                = "Terraform provisioned Virtual Server"
  access_log_enabled         = true
  application_profile_path   = "/infra/lb-app-profiles/default-tcp-lb-app-profile"
  enabled                    = true
  ip_address                 = var.kube_api_ip
  ports                      = ["443"]
  service_path               = nsxt_policy_lb_service.kubernetes.path
  pool_path                  = nsxt_policy_lb_pool.ingress_https.path
}

resource "nsxt_policy_lb_virtual_server" "ingress_http" {
  display_name               = "${var.cluster_name}_ingress_http"
  description                = "Terraform provisioned Virtual Server"
  access_log_enabled         = true
  application_profile_path   = "/infra/lb-app-profiles/default-tcp-lb-app-profile"
  enabled                    = true
  ip_address                 = var.kube_api_ip
  ports                      = ["80"]
  service_path               = nsxt_policy_lb_service.kubernetes.path
  pool_path                  = nsxt_policy_lb_pool.ingress_https.path
}

# ### Kube API Load Balancer

# resource "nsxt_lb_service" "lb_service" {
#   description  = "kubernetes load balancer"
#   display_name = "kubernetes"

#   enabled           = true
#   logical_router_id = data.nsxt_logical_tier1_router.tier1_router.id
#   error_log_level   = "INFO"
#   size              = "SMALL"

#   virtual_server_ids = [nsxt_lb_tcp_virtual_server.kube_api.id, nsxt_lb_tcp_virtual_server.ingress_https.id, nsxt_lb_tcp_virtual_server.ingress_http.id]
# }

# # Kube API Master Group
# resource "nsxt_ns_group" "kube_api" {
#     description     = "Kube API for ${var.cluster_name}"
#     display_name    = "Kube-API-${var.cluster_name}"

#     membership_criteria {
#         target_type = "VirtualMachine"
#         scope       = "k8s/${var.cluster_name}"
#         tag         = "master"
#     }
# }

# resource "nsxt_ns_group" "ingress-https" {
#     description     = "https ingress for ${var.cluster_name}"
#     display_name    = "ingress-https-${var.cluster_name}"

#     membership_criteria {
#         target_type = "VirtualMachine"
#         scope       = "k8s/${var.cluster_name}"
#         tag         = "worker"
#     }
# }

# resource "nsxt_ns_group" "ingress-http" {
#     description     = "http ingress for ${var.cluster_name}"
#     display_name    = "ingress-http-${var.cluster_name}"

#     membership_criteria {
#         target_type = "VirtualMachine"
#         scope       = "k8s/${var.cluster_name}"
#         tag         = "worker"
#     }
# }

# resource "nsxt_lb_tcp_monitor" "kube_api_tcp_monitor" {
#   description  = "Health Monitor for Kube API Servers"
#   display_name = "kubeapi_6443_tcp_monitor-${var.cluster_name}"
#   fall_count   = 3
#   interval     = 5
#   monitor_port = 6443
#   rise_count   = 3
#   timeout      = 10
# }

# resource "nsxt_lb_tcp_monitor" "ingress_https_tcp_monitor" {
#   description  = "Health Monitor for ingress https Servers"
#   display_name = "ingress_443_tcp_monitor-${var.cluster_name}"
#   fall_count   = 3
#   interval     = 5
#   monitor_port = 443
#   rise_count   = 3
#   timeout      = 10
# }

# resource "nsxt_lb_tcp_monitor" "ingress_http_tcp_monitor" {
#   description  = "Health Monitor for ingress http Servers"
#   display_name = "ingress_80_tcp_monitor-${var.cluster_name}"
#   fall_count   = 3
#   interval     = 5
#   monitor_port = 80
#   rise_count   = 3
#   timeout      = 10
# }

# resource "nsxt_lb_fast_tcp_application_profile" "lb_fast_tcp_profile" {
#   description       = "lb_fast_tcp_application_profile provisioned by Terraform"
#   display_name      = "${var.cluster_name}_lb_fast_tcp_application_profile"
#   close_timeout     = "8"
#   idle_timeout      = "1800"
#   ha_flow_mirroring = "false"
# }

# # Kube API LB Pool
# resource "nsxt_lb_pool" "kube_api_pool" {
#     description     = "Pool for ${var.cluster_name} KUBEAPI"
#     display_name    = "${var.cluster_name}-kubeapi"
    
#     snat_translation {
#         type = "SNAT_AUTO_MAP"
#     }
    
#     active_monitor_id = nsxt_lb_tcp_monitor.kube_api_tcp_monitor.id

#     member_group {
#         ip_version_filter = "IPV4"
#         port              = "6443"

#         grouping_object {
#             target_type = "NSGroup"
#             target_id   = nsxt_ns_group.kube_api.id
#         }
#     }
# }

# resource "nsxt_lb_pool" "ingress_https_pool" {
#     description     = "Pool for ${var.cluster_name} HTTPS ingress"
#     display_name    = "${var.cluster_name}-ingress-https"
    
#     snat_translation {
#         type = "SNAT_AUTO_MAP"
#     }
    
#     active_monitor_id = nsxt_lb_tcp_monitor.ingress_https_tcp_monitor.id

#     member_group {
#         ip_version_filter = "IPV4"
#         port              = "443"

#         grouping_object {
#             target_type = "NSGroup"
#             target_id   = nsxt_ns_group.ingress-https.id
#         }
#     }
# }

# resource "nsxt_lb_pool" "ingress_http_pool" {
#     description     = "Pool for ${var.cluster_name} HTTP ingress"
#     display_name    = "${var.cluster_name}-ingress-http"
    
#     snat_translation {
#         type = "SNAT_AUTO_MAP"
#     }
    
#     active_monitor_id = nsxt_lb_tcp_monitor.ingress_http_tcp_monitor.id

#     member_group {
#         ip_version_filter = "IPV4"
#         port              = "80"

#         grouping_object {
#             target_type = "NSGroup"
#             target_id   = nsxt_ns_group.ingress-http.id
#         }
#     }
# }

# # Kube API Virtual Server
# resource "nsxt_lb_tcp_virtual_server" "kube_api" {
#     description = "Virtual Server for ${var.cluster_name} Kube API"
#     display_name = "${var.cluster_name}-kubeapi"
#     enabled = true
#     ip_address = var.kube_api_ip
#     ports = ["6443"]
#     pool_id = nsxt_lb_pool.kube_api_pool.id
#     application_profile_id = nsxt_lb_fast_tcp_application_profile.lb_fast_tcp_profile.id
# }

# resource "nsxt_lb_tcp_virtual_server" "ingress_https" {
#     description = "Virtual Server for ${var.cluster_name} HTTPS Ingress"
#     display_name = "${var.cluster_name}-ingress-https"
#     enabled = true
#     ip_address = var.kube_api_ip
#     ports = ["443"]
#     pool_id = nsxt_lb_pool.ingress_https_pool.id
#     application_profile_id = nsxt_lb_fast_tcp_application_profile.lb_fast_tcp_profile.id
# }

# resource "nsxt_lb_tcp_virtual_server" "ingress_http" {
#     description = "Virtual Server for ${var.cluster_name} HTTP Ingress"
#     display_name = "${var.cluster_name}-ingress-http"
#     enabled = true
#     ip_address = var.kube_api_ip
#     ports = ["80"]
#     pool_id = nsxt_lb_pool.ingress_http_pool.id
#     application_profile_id = nsxt_lb_fast_tcp_application_profile.lb_fast_tcp_profile.id
# }

# resource "nsxt_vm_tags" "rke" {
#     count = var.rancher_count_master
#     instance_id = vsphere_virtual_machine.rke[count.index].id
#     tag {
#         scope = "k8s/${var.cluster_name}"
#         tag = "master"
#     }
#     tag {
#         scope = "k8s/${var.cluster_name}"
#         tag = "worker"
#     }
# }

