data "template_file" "csi-vsphere" {
    template = file("templates/csi-vsphere.conf.tpl")
    vars = {
        vsphere_user = var.vsphere_user
        vsphere_password = var.vsphere_password
        vsphere_vcenter = var.vsphere_vcenter
        vsphere_datacenter = var.vsphere_datacenter
        cluster_name = var.cluster_name
    }
}

data "template_file" "rke_addons" {
    template = file("templates/rke-addons.tpl")
    vars = {
        vsphere_user = base64encode(var.vsphere_user)
        vsphere_password = base64encode(var.vsphere_password)
        vsphere_vcenter = var.vsphere_vcenter
        vsphere_datacenter = var.vsphere_datacenter
        csi-vsphere = base64encode(data.template_file.csi-vsphere.rendered)
    }
}

resource "rke_cluster" "cluster" {
  cluster_name = var.cluster_name

  dynamic nodes {
    for_each = vsphere_virtual_machine.rke
    content {
      hostname_override = nodes.value.name
      address = nodes.value.default_ip_address
      user = "estork"
      role = ["controlplane", "etcd", "worker"]
      labels = {
          "node-role.kubernetes.io/master" = ""
      }
    }
  }

  ssh_key_path = var.ssh_private_key

  network {
      plugin = "canal"
  }

  authentication {
    strategy = "x509"

    sans = [
      var.kube_api_ip,
      "rancher-server.hawaiian.islands.io",
    ]
  }
  services {
      kubelet {
          extra_args = {
              "cloud-provider" = "external"
          }
      }
  }

  addons = data.template_file.rke_addons.rendered
}