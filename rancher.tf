provider "rancher2" {
  api_url = "https://${var.rancher_api_url}"
  bootstrap = true
  insecure = true
}

resource "rancher2_bootstrap" "admin" {
  depends_on = [helm_release.rancher]
  password = var.rancher_password
  telemetry = true
}